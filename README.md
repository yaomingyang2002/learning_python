About this repository and the project:

The main aim of this project is to create a repository for quick review and refresh Python 3 knowledge online.

This repository covers most essential topics for Python 3, new topic will be added if need, and can function as a complete code reference for learning Python 3.

All codes in this repo are up-to-date Python 3 codes and have been checked in Python console.



Getting Started:

There are two approches for getting started with and using this repo.

1. Online code review and refresh:

Follow the step-by-step commit working flow or read code for specific topic.

2. Git clone or download this repo, open to run Python 3 IDE or your terminal to review these Python 3 codes.