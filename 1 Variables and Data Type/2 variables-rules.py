"""
variables rules
1 Reserved Keywords cannot be used as the name of the variables:
['False', 'None', 'True', 'and', 'as', 'assert', 'break', 'class', 'continue', 'def',
'del', 'elif', 'else', 'except', 'finally', 'for', 'from', 'global', 'if', 'import',
'in', 'is', 'lambda', 'nonlocal', 'not', 'or', 'pass', 'raise', 'return', 'try',
'while', 'with', 'yield']
2 Start with letter or	underscore
3 Don’t start with a number
4 Variable name can	contain letters, number,underscore
5 Don’t	include	special	characters in variable names
"""

import keyword
print(keyword.kwlist)

a = b = c = 10
print(a)
print(b)
print(c)

x, y, z = 10, 20, 30
print(x)
print(y)
print(z)